import { Injectable } from '@angular/core';
import { UserserService } from './service/userser.service';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private _dalObj: UserserService) { }

  savedata(_request: string) {
    return this._dalObj.PostMethod('User/SaveData', _request);
  }
}
