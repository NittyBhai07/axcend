import { Component, ViewChildren } from '@angular/core';
import { User } from './user';
import { RestService } from './rest.service';
import { CreatejsonService } from './createjson.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private _restServies: RestService,
    private _json: CreatejsonService, ) { }
  title = 'angular8csv'; 
  NoofStudents:number;
  Message:string; 
  csvArr = [];
  _userRequest: string;
  EmployeeIDs : string;
  usersList = [];
  searchText: string;

  public records: any[] = [];  
  @ViewChildren('csvReader') csvReader: any;  
  
  uploadListener($event: any): void {
    debugger;  
  
    let text = [];  
    let files = $event.srcElement.files;  
  
    if (this.isValidCSVFile(files[0])) {  
  
      let input = $event.target;  
      let reader = new FileReader();  
      reader.readAsText(input.files[0]);  
  
      reader.onload = () => {  
        let csvData = reader.result;  
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  
  
        let headersRow = this.getHeaderArray(csvRecordsArray);  
  
        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);  
      };  
  
      reader.onerror = function () {  
        console.log('error is occured while reading file!');  
      };  
  
    } else {  
      alert("Please import valid .csv file.");  
      this.fileReset();  
    }  
  }  
  
  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
    debugger;
      
  
    for (let i = 1; i < csvRecordsArray.length; i++) {  
      let curruntRecord = (<string>csvRecordsArray[i]).split(',');  
      if (curruntRecord.length == headerLength) {  
        let csvRecord: User = new User();  
        csvRecord.Name = curruntRecord[0].trim();  
        csvRecord.Age = curruntRecord[1].trim();  
        csvRecord.City = curruntRecord[2].trim();  
        this.csvArr.push(csvRecord);
        this.NoofStudents = this.csvArr.length;  
      }  
    }  
    return this.csvArr;  
  }  
  
  isValidCSVFile(file: any) {  
    return file.name.endsWith(".csv");  
  }  
  
  getHeaderArray(csvRecordsArr: any) {  
    let headers = (<string>csvRecordsArr[0]).split(',');  
    let headerArray = [];  
    for (let j = 0; j < headers.length; j++) {  
      headerArray.push(headers[j]);  
    }  
    return headerArray;  
  }  
  
  fileReset() {  
    this.csvReader.nativeElement.value = "";  
    this.records = []; 
  }  

  Savenotice() {
    debugger;
    this._userRequest = this._json.GenerateJson(this.csvArr);
    this._restServies.savedata(this._userRequest).subscribe(
      res => {
        debugger;
        if (res.ResponseCode === '000') {
          this.Message='success';
        } else {
          //this._toast.dangerToast('Failed to send Message');
        }
      },
    );
  }
}
