import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CreatejsonService {

  constructor() { }

  requestheader = {
    SourceID: ''
  };

  GenerateJson(model: any): string {
    let jsonData = '{\"RequestHeader\":' + JSON.stringify(this.requestheader);
    jsonData = jsonData + ',\"RequestData\":' + JSON.stringify(model) + '} ';
    return jsonData;
  }
}
