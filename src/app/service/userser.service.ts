import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../user';


@Injectable({
  providedIn: 'root'
})
export class UserserService {

  constructor(private http: HttpClient) { }

  _api: string;

  //Local
  _defaultApi: string = 'http://localhost:58578/api/';

  ResponseData: any;

  _httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  PostMethod(_endPoint: string, _jsonRequest: string): Observable<any> {
    this._api = this._defaultApi + _endPoint;
    return this.http.post<any>(this._api, _jsonRequest, this._httpOptions)
      .pipe(map(
        res => {
          let temp = res.ResponseData;
          return temp;
        }
      ));
  } 
}
